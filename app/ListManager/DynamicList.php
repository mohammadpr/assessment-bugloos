<?php

namespace App\ListManager;

use App\Models\User;
use Illuminate\Support\Facades\DB;


class DynamicList
{
    protected $gateway;

    public function gateway(Gateway $gateway)
    {
        $this->gateway = $gateway;
        $this->gateway->setData();
    }

    public function get()
    {
        return $this->gateway->get();
    }

    public function search($field, $text)
    {
        $this->gateway->search($field, $text);
    }

    public function sort($field, $sort_by)
    {
        $this->gateway->sort($field, $sort_by);
    }
}


