<?php

namespace App\ListManager;

use Illuminate\Support\Facades\DB;

class DatabaseService extends Gateway
{

    public function setData()
    {
        $this->data = DB::table($this->source);
    }

    public function get()
    {
        $fields_name = array_column($this->fields, 'name');

        if ($this->pert_page){
            $per_page = $this->pert_page;
        }else{
            $per_page = 10;
        }
        return $this->data->select($fields_name)->paginate($per_page);
    }

    public function search($field, $text)
    {
        if ($field && $text){
            $this->data = $this->data->where($field,'like', '%' .$text.'%');
        }

    }


    public function sort($field, $sort_by)
    {
        if ($field && $sort_by) {
            $this->data = $this->data->orderBy($field, $sort_by);
        }
    }
}
