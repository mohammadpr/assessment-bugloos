<?php

namespace App\ListManager;

class FieldList
{
    private $fields = [];

    public function addField(Field $field)
    {
        $this->fields[] = $field;
    }

    public function get()
    {
        return $this->fields;
    }



}
