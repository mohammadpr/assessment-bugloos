<?php

namespace App\ListManager;

use phpDocumentor\Reflection\Types\Boolean;

class Field
{

    public $name,$width, $title, $sortable,$searchable;

    public function __construct($name, $title, $width,bool $sortable,bool $searchable)
    {
        $this->name = $name;
        $this->title = $title;
        $this->width = $width;
        $this->sortable = $sortable;  $this->searchable = $searchable;
    }

    public function field()
    {
        return ["name" => $this->name, "title" => $this->title, "sortable" => $this->sortable,"searchable" => $this->searchable, "width" => $this->width];
    }

}
