<?php

namespace App\ListManager;

//this class chose gateway  Database or Webservice ,...
abstract class Gateway
{

    public function __construct($fields, $source,$pert_page)
    {
        $this->fields = $fields;
        $this->source = $source;
        $this->pert_page = $pert_page;
    }
    abstract public function setData();
    abstract public function get();

    abstract protected function search($field, $text);

    abstract protected function sort($field, $sort_by);
}
