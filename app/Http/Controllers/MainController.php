<?php

namespace App\Http\Controllers;

use App\ListManager\DatabaseService;
use App\ListManager\DynamicList;
use App\ListManager\Field;

use App\ListManager\FieldList;
use App\Models\User;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(Request $request)
    {


        $fields = $this->getFields();
        $dynamic_list = new DynamicList();
        $dynamic_list->gateway(new DatabaseService($fields, 'users',$request->per_page));
        $dynamic_list->search($request->search_field,$request->search_text);
        $dynamic_list->sort($request->sort_field,$request->sort_by);
        $data = $dynamic_list->get();
        return view('welcome', compact('data', 'fields'));
    }


    public function getFields()
    {
        $fields = new FieldList();
        $fields->addField(new Field('id', '#', 30, true, false));
        $fields->addField(new Field('name', 'Name', 10, true, true));
        $fields->addField(new Field('email', 'Email', 100, false, true));
        $fields->addField(new Field('created_at', 'Time', 100, true, false));
        return $fields->get();
    }
}
