<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_that_true_is_true()
    {
        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function test_for_set_param_in_form()
    {
        $response = $this->get('/?per_page=4&sort_field=created_at&sort_by=DESC&search_field=name&search_text=salamsalam&page=1');
        $response->assertSee('DESC');
        $response->assertSee('salamsalam');
    }
}
